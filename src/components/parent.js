import React, { Component } from "react";
import Display from '../components/display';
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            display: false,
            class: "btn btn-info",
            quote: 'Create component',
        }
    }
    buttonHandler = () => {
        switch (this.state.display) {
            case false:
                this.setState({
                    display: true,
                    class: "btn btn-danger",
                    quote: 'Destroy component',
                })
                break;
            case true:
                this.setState({
                    display: false,
                    class: "btn btn-info",
                    quote: 'Create component',
                })
                break;
            default:
                this.state = {
                    display: false,
                    class: "btn btn-info",
                    quote: 'Create component',
                }
                break;
        }
    }
    render() {
        return (
            <div className="container mt-5 text-center">
                <button className={this.state.class} onClick={this.buttonHandler}>{this.state.quote}</button>
                <div> {this.state.display ? <Display /> : null} </div>
            </div>
        )
    }
}

export default Parent;