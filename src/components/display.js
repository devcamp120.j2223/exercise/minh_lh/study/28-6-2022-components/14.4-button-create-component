import React, { Component } from "react";

class Display extends Component {
    componentWillMount() {
        console.log('Component Will Mount');
    }
    componentDidMount() {
        console.log('Component Did Mount');
    }
    componentWillUnmount() {
        console.log("Component will unmount");
    }
    render() {
        return (
            <h1 style={{color:"green"}}>I exist !</h1>
        )
    }
}

export default Display;